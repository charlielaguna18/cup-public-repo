

		Basic retexture config template for CUP assets
        by audiocustoms



Before we start, setup your P Drive and install Mikero's Tools
How to do that correctly (no Arma 3 Tools) can be read here:

https://pmc.editing.wiki/doku.php?id=arma3:tools:setup-p-drive

Once that is done, copy the folder "ABC_CUP_Retextures" and all it's content to your newly created P Drive.
You can now rename this folder to whatever you like. The purpose of this folder is to have a clean
folder structure on your workspace. It is recommended to follow the this template:

PREFIX_CONTENT

PREFIX:         Your artist prefix. It usualy contains 3 letters and often is
                an abbreviation of your nickname.
CONTENT:		What is inside thos folder, what project is it.


The folder "ABC_CUP_Retextures" contains subfolders for every model you are retexturing,
and a folder for common / shared files.
In this case it's a folder for the Bell 412 (by the time of writing, the Bell 412 is only in
CUPs DEV branch available) and "ABC_CUP_Retextures_Data". Create a new folder for every model you add.
Multiple retextures of one model can stay in the same main model folder.

Once you are done with your retextures, save them as TGA and then use the ImageToPAA converter
in the Arma 3 tools to save them as PAA files. Copy the PAA textures to the "skins" folder inside
your model folder.
Create a logo for you mod (1024x1024px) and also convert it to PAA. Copy your "logo.paa" to the UI folder
inside "ABC_CUP_Retextures_Data" (you can rename this folder to your prefix also, just keep in mind that
you need the paths later).

Now it's time to edit the config.
Open the config.cpp in a text editor (recommended are Sublime2 or Notepad++) and make your changes.
IMPORTANT: Doublecheck your paths to the texture files!!!


Packing and testing your new mod:

1.	Go to the folder where your mods are
2.	Create a new folder and call it "@PREFIX_YOURMODNAME"
3.	Open that folder and create a new file. Name it "mod.cpp"
4.	Copy, paste this text to the "mod.cpp" file, then edit it's entries to your specifications:

	name = "MY MOD NAME";
	picture = "\ABC_CUP_Retextures\ABC_CUP_Retextures_Data\UI\logo.paa";
	actionName = "Website";
	action = "http://cup-arma3.org";
	logo = "\ABC_CUP_Retextures\ABC_CUP_Retextures_Data\UI\logo.paa";
	logoOver = "\ABC_CUP_Retextures\ABC_CUP_Retextures_Data\UI\logo.paa";
	logoSmall = "\ABC_CUP_Retextures\ABC_CUP_Retextures_Data\UI\logo.paa";
	tooltip = "YOUR MOD NAME";
	tooltipOwned = "YOUR MOD NAME";
	overview = "SOME INFO ABOUT YOUR MOD";
	author = "YOUR NAME";
	overviewPicture = "\ABC_CUP_Retextures\ABC_CUP_Retextures_Data\UI\logo.paa";
	overviewText = "YOUR MOD NAME";
	overviewFootnote = "";

5.	IMPORTANT: Again, check your paths!!!
6.	Open pboProject from Mikero's tools
7. 	Open "Setup"
8.	Uncheck "Warnings are errors"
9.	Press "OK"
10.	Open "Mod Folder Output" and select your new "´@PREFIX_YOURMODNAME" folder
11.	Open "Source Folder" and select your project folder on your P Drive (P:\ABC_CUP_Retextures)
12. Press "Crunch" and hope for the best.


If the build fails, check the CMD window for information. Also press "View Output(s)".
This will open a text file with error information about missing files ect.
If you get a missing file error in the log, check your paths and file names again!!

Once the build is successful, open the arma 3 launcher, navigate to the Mods tab and load your new mod.
Run the game and check the result in game.

WELCOME TO THE HELL OF ARMA MODDING!


BTW, reading and following this tutorial costs either #350, your soul or your first born.